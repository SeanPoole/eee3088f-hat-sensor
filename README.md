# EEE3088F HAT Sensor
A repo with all the documentation and files relating to the vibration and acceleration sensing HAT.
This repo will be continously updated 

## Name
Acceleration and Vibration monitoring HAT.

## Pr0ject Description
A vibration and acceleration monitor HAT for the STM - Dsicovery Board. The board will provide key infomration relating to the sensors. It aims to provide people with the ability to monitor the switching on and off of vehicles as well as monotoring acceleration. The HAT can be used in other contexts and is not limited to vehicles.

## Hardware & Software Requirements
- A computer with Windows or Mac OS
- STM32Cube IDE
- Serial port moniter (Putty Software)
- STM Discovery board with STM32F051x8 Micro-Controller chip
- A MircoUSB to USB-A Cable 
- A MiniUSB to USB-A Cable
- A Male to Male Jumper Cables
- 10x Female to Male Jumper Cables

## How to Install the HAT
Attach MircoUSB to USB-A Cable to the HAT Board for Powering the board
Attach MiniUSB to USB-A Cable to the STM320F051 Discovery Board for Data line
Attach the 8 Jumper cables from the STM board to the HAT
Computer should 

## How to bypass UVLO
The Under Voltage protection within the HAT's is currently not working in this itteraton of the project. To short the UVLO, you must:
- Solder the extra jumper from Test Pad 6 (TP6 on the board)
- Solder the other end of the jumber to Test Pad 4 (TP4 on the board)
This effectively Bypass the UVLO entirely from the output off the batery polarity protection thorugh tot he input of the 3.3V Voltage Regulator.   

## How to Contribute to the Repo
Note Contributing.md

## Future Roadmap
Continous improvements tot he HAT in order to obtain a fully functioning Hat which is completely capabale of producing usable data. Currently this desgin was overtaken over 12 weeks, further anaylsis of the current model and it's capabilites will be reqorked in the short Future

## Authors and acknowledgment
Authors:
Kamryn Norton (NRTKAM001)
Sean Poole (PLXSEA003)
Khavish Govind (GVNKHA001)

## License
For open source projects, say how it is licensed.

