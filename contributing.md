# Welcome to GitHub docs contributing guide <!-- omit in toc -->

Thank you for investing your time in contributing to our project! Any contribution you make will be reflected on [docs.github.com](https://docs.github.com/en) :sparkles:. 

Read our [Code of Conduct](./CODE_OF_CONDUCT.md) to keep our community approachable and respectable.

In this guide you will get an overview of the contribution workflow from opening an issue, creating a pull request(PR), reviewing, and merging the PR.

## New contributor guide

To get an overview of the project, read the README.md. 

## Getting started

Check to see what types of contributions we accept before making changes. As certain languages will cause merging errors or corrupted files

### Issues

As no project is perfect here are the methods as to how to highlight an issue or solve an exisitng once

#### Create a new issue

If you spot a problem with the docs, check the exisitng issues to see if it already exists. If the issue doesn't exist, you can open a new issue.

#### Solve an issue

Scan through our existing issues. If you find an issue that interests you to work on, open a pull request with a fix.

#### Make changes locally

1. Install Git
2. Fork the repository.
- Using the command line:
  - Setuup Github and fork the repo so that you can make your changes without affecting the original project until you're ready to merge them.
3. Create a working branch and start with your changes!

### Commit your update

Commit the changes once you are happy with them.
Once your changes are ready, don't forget to self-reviewto speed up the review process

### Pull Request

When you're finished with the changes, create a pull request, also known as a PR.
- Fill the "Ready for review" template so that we can review your PR. This template helps reviewers understand your changes as well as the purpose of your pull request. 
- Enable the checkbox to allow maintainer edits so the branch can be updated for a merge.
Once you submit your PR, the team member will review your proposal. We may ask questions or request for additional information.
- We may ask for changes to be made before a PR can be merged, either using suggested changes or pull request comments. You can apply suggested changes directly through the UI. You can make any other changes in your fork, then commit them to your branch.
- As you update your PR and apply changes, mark each conversation as resolved
- If you run into any merge issues, check forums or tutorials on how to help you resolve merge conflicts and other issues.

### Approved PR
Once a team member has amade a full review of your PR and decided on its validity and the issue which is addresses, the PR request will be approved and your edits will be made.

