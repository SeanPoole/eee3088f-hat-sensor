PCBNEW-LibModule-V1  2022-03-17 09:15:45
# encoding utf-8
Units mm
$INDEX
RESC2013X65N
$EndINDEX
$MODULE RESC2013X65N
Po 0 0 0 15 6232fc41 00000000 ~~
Li RESC2013X65N
Cd 0805W8F4701T5E
Kw Resistor
Sc 0
At SMD
AR 
Op 0 0 0
T0 0 0 1.27 1.27 0 0.254 N V 21 N "R**"
T1 0 0 1.27 1.27 0 0.254 N I 21 N "RESC2013X65N"
DS -1.7 -1 1.7 -1 0.05 24
DS 1.7 -1 1.7 1 0.05 24
DS 1.7 1 -1.7 1 0.05 24
DS -1.7 1 -1.7 -1 0.05 24
DS -1 -0.638 1 -0.638 0.1 24
DS 1 -0.638 1 0.638 0.1 24
DS 1 0.638 -1 0.638 0.1 24
DS -1 0.638 -1 -0.638 0.1 24
DS 0 -0.538 0 0.538 0.2 21
$PAD
Po -0.95 0
Sh "1" R 1 1.45 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.95 0
Sh "2" R 1 1.45 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$EndMODULE RESC2013X65N
$EndLIBRARY
