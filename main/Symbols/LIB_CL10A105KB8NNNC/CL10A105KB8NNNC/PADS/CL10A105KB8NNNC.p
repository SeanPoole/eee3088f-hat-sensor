*PADS-LIBRARY-PART-TYPES-V9*

CL10A105KB8NNNC CAPC1608X90N I CAP 9 1 0 0 0
TIMESTAMP 2022.03.17.08.29.07
"Manufacturer_Name" Samsung Electro-Mechanics
"Manufacturer_Part_Number" CL10A105KB8NNNC
"Mouser Part Number" 187-CL10A105KB8NNNC
"Mouser Price/Stock" https://www.mouser.co.uk/ProductDetail/Samsung-Electro-Mechanics/CL10A105KB8NNNC?qs=ktYoCbbtCSJz4pFCLWXd5g%3D%3D
"Arrow Part Number" CL10A105KB8NNNC
"Arrow Price/Stock" https://www.arrow.com/en/products/cl10a105kb8nnnc/samsung-electro-mechanics?region=europe
"Description" CAP, 1uF, 50V, +/-10%, X5R, 0603
"Datasheet Link" https://datasheet.datasheetarchive.com/originals/distributors/Datasheets-DGA10/2411562.pdf
"Geometry.Height" 0.9mm
GATE 1 2 0
CL10A105KB8NNNC
1 0 U 1
2 0 U 2

*END*
*REMARK* SamacSys ECAD Model
775975/775245/2.49/2/3/Capacitor
