(part "0603WAF1001T5E"
    (packageRef "RESC1608X55N")
    (interface
        (port "1" (symbPinId 1) (portName "1") (portType INOUT))
        (port "2" (symbPinId 2) (portName "2") (portType INOUT))
    )
    (partClass UNDEF)
    (useInSchema Y)
    (useInLayout Y)
    (inPartsList Y)
    (partType NORMAL)
    (placeRestriction FREE)
    (property "compKind" "131")
    (property "Manufacturer_Name" "UNI-ROYAL(Uniroyal Elec)")
    (property "Manufacturer_Part_Number" "0603WAF1001T5E")
    (property "Mouser_Part_Number" "")
    (property "Mouser_Price/Stock" "")
    (property "Arrow_Part_Number" "")
    (property "Arrow_Price/Stock" "")
    (property "Description" "1k +/-1% 0.1W +/-100ppm/ 0603 Chip Resistor - Surface Mount")
    (property "Datasheet_Link" "https://datasheet.lcsc.com/szlcsc/Uniroyal-Elec-0603WAF1001T5E_C21190.pdf")
    (property "symbolName1" "0603WAF1001T5E")
)
