*PADS-LIBRARY-PART-TYPES-V9*

0402WGF100KTCE RESC1005X40N I RES 9 1 0 0 0
TIMESTAMP 2022.03.17.09.31.58
"Manufacturer_Name" Uniroyal Elec
"Manufacturer_Part_Number" 0402WGF100KTCE
"Mouser Part Number" 
"Mouser Price/Stock" 
"Arrow Part Number" 
"Arrow Price/Stock" 
"Description" 1 +/-1% 1/16W +/-400ppm/ 0402 Chip Resistor - Surface Mount RoHS
"Datasheet Link" https://datasheet.lcsc.com/szlcsc/Uniroyal-Elec-0402WGF100KTCE_C25086.pdf
"Geometry.Height" 0.4mm
GATE 1 2 0
0402WGF100KTCE
1 0 U 1
2 0 U 2

*END*
*REMARK* SamacSys ECAD Model
13608507/775245/2.49/2/3/Resistor
