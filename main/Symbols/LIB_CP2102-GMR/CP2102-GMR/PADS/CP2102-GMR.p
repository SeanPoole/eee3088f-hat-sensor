*PADS-LIBRARY-PART-TYPES-V9*

CP2102-GMR QFN50P500X500X100-29N-D I ANA 9 1 0 0 0
TIMESTAMP 2022.03.03.12.55.44
"Manufacturer_Name" Silicon Labs
"Manufacturer_Part_Number" CP2102-GMR
"Mouser Part Number" 634-CP2102-GMR
"Mouser Price/Stock" https://www.mouser.co.uk/ProductDetail/Silicon-Labs/CP2102-GMR?qs=cNyzPCcHMUWu%2FiPhR8KQAw%3D%3D
"Arrow Part Number" CP2102-GMR
"Arrow Price/Stock" https://www.arrow.com/en/products/cp2102-gmr/silicon-labs?region=nac
"Description" I/O Controller Interface IC USB-TO-UART BRIDGE, qfn-28
"Datasheet Link" https://www.silabs.com/Support%20Documents/TechnicalDocs/CP2102-9.pdf
"Geometry.Height" 1mm
GATE 1 29 0
CP2102-GMR
1 0 U DCD
2 0 U RI
3 0 U GND
4 0 U D+
5 0 U D-
6 0 U VDD
7 0 U REGIN
8 0 U VBUS
9 0 U \RST
10 0 U NC_1
11 0 U \SUSPEND
12 0 U SUSPEND
13 0 U NC_2
14 0 U NC_3
21 0 U NC_9
20 0 U NC_8
19 0 U NC_7
18 0 U NC/VPP
17 0 U NC_6
16 0 U NC_5
15 0 U NC_4
29 0 U TPAD
28 0 U DTR
27 0 U DSR
26 0 U TXD
25 0 U RXD
24 0 U RTS
23 0 U CTS
22 0 U NC_10

*END*
*REMARK* SamacSys ECAD Model
236911/775093/2.49/29/0/Integrated Circuit
